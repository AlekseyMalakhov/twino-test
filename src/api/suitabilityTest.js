import baseURL from "./baseURL";

let groups = [];

const getAnswers = () => {
    const answersString = localStorage.getItem("suitabilityTestAnswers");
    return JSON.parse(answersString);
};

const createAnswersArray = (groups) => {
    const answers = getAnswers();
    if (!answers) {
        const arr = groups.map((group) => {
            return group.questions.map(() => null);
        });
        localStorage.setItem("suitabilityTestAnswers", JSON.stringify(arr));
    }
};

const getAllQuizGroups = () => {
    return fetch(baseURL + "/suitability")
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            groups = data.groups;
            createAnswersArray(groups);
            return data.groups;
        });
};

const getGroups = () => {
    return groups;
};

const sendAnswers = (answers) => {
    localStorage.setItem("suitabilityTestAnswers", JSON.stringify(answers));
};

export { getAllQuizGroups, getGroups, sendAnswers, getAnswers };
