export default {
    grey: "#e0dada",
    green: "#72ca72",
    lightGreen: "#c3fdc3",
    blue: "#5153DB",
    darkBlue: "#2A2CAC",
    red: "tomato",
    lightPink: "#fde7eb",
};
