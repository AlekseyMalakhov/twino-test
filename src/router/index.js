import { createRouter, createWebHistory } from "vue-router";
import Question from "../views/Question.vue";
import Overview from "../views/Overview.vue";

const routes = [
    {
        path: "/",
        redirect: "/test/1/1",
    },
    {
        path: "/test/:currentGroupId/:question_id",
        name: "Test",
        component: Question,
    },
    {
        path: "/overview",
        name: "Overview",
        component: Overview,
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
