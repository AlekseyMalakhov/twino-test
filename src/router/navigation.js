import router from "./index";
import { getGroups } from "../api/suitabilityTest";

const getInfo = () => {
    let currentGroupId = Number.parseInt(router.currentRoute.value.params.currentGroupId);
    let questionId = Number.parseInt(router.currentRoute.value.params.question_id);

    const groups = getGroups();
    const groupsNumber = groups.length;
    const questionsNumber = groups[currentGroupId - 1].questions.length;

    return { currentGroupId, questionId, groupsNumber, questionsNumber, groups };
};

export const nextQuestion = () => {
    let { currentGroupId, questionId, groupsNumber, questionsNumber } = getInfo();

    if (currentGroupId === groupsNumber && questionId === questionsNumber) {
        console.log("top");
        router.push("/overview");
        return;
    }

    if (questionId === questionsNumber) {
        currentGroupId++;
        questionId = 1;
    } else {
        questionId++;
    }

    router.push(`/test/${currentGroupId}/${questionId}`);
};

export const previousQuestion = () => {
    let { currentGroupId, questionId, groups } = getInfo();

    if (currentGroupId === 1 && questionId === 1) {
        console.log("stop");
        return;
    }
    if (questionId === 1) {
        currentGroupId--;
        questionId = groups[currentGroupId - 1].questions.length;
    } else {
        questionId--;
    }

    router.push(`/test/${currentGroupId}/${questionId}`);
};
