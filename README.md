## Aleksey Malakhov test for TWINO

At first please start server from api_server folder

```
npm start
```

It will serve data for Vue app.
Then start Vue app from the root folder

```
npm run serve
```

or

```
vue ui
```

To run tests

```
npm run test:unit
```
