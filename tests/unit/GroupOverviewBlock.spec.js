import { shallowMount } from "@vue/test-utils";
import GroupOverviewBlock from "@/components/GroupOverviewBlock.vue";

describe("GroupOverviewBlock.vue", () => {
    it("should render", async () => {
        const wrapper = shallowMount(GroupOverviewBlock, {
            props: {
                group: {
                    title: "Test title 1",
                    questions: [
                        {
                            question: "Test question 1",
                            answers: ["Answer 1", "Answer 2", "Answer 3"],
                        },
                        {
                            question: "Test question 2",
                            answers: ["Answer 4", "Answer 5", "Answer 6"],
                        },
                        {
                            question: "Test question 3",
                            answers: ["Answer 7", "Answer 8", "Answer 9"],
                        },
                    ],
                },
                answers: [0, 2, 1],
            },
        });

        const title = wrapper.find(".top").text();
        expect(title).toBe("Test title 1");

        await wrapper.vm.toggle("opened");
        const bottom = wrapper.find(".bottom");
        expect(bottom.isVisible()).toBe(true);

        const overviewResults = wrapper.findAllComponents({ name: "OverviewResult" });
        expect(overviewResults.length).toBe(3);
    });
});
