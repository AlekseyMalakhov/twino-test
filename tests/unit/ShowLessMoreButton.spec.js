import { shallowMount } from "@vue/test-utils";
import ShowLessMoreButton from "@/components/ShowLessMoreButton.vue";

describe("ShowLessMoreButton.vue", () => {
    it("should render", async () => {
        const wrapper = shallowMount(ShowLessMoreButton);

        const button = wrapper.find(".containerButton");
        await button.trigger("click");

        const text = button.text();
        expect(text).toBe("Show less");
    });
});
