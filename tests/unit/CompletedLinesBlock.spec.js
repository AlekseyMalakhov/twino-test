import { shallowMount } from "@vue/test-utils";
import CompletedLinesBlock from "@/components/CompletedLinesBlock.vue";

jest.mock("vue-router");

describe("CompletedLinesBlock.vue", () => {
    it("should render", () => {
        require("vue-router").useRoute.mockReturnValueOnce({ path: "/test/2/3" });
        const wrapper = shallowMount(CompletedLinesBlock, {
            props: {
                currentGroupId: 0,
                questionId: 0,
                done: 50,
                title: "Test title 1",
            },
            global: {
                provide: {
                    groups: {
                        value: [
                            {
                                title: "Mock 1",
                                questions: [
                                    {
                                        question: "Test question 1",
                                        answers: ["Answer 1", "Answer 2", "Answer 3"],
                                    },
                                    {
                                        question: "Test question 2",
                                        answers: ["Answer 1", "Answer 2", "Answer 3"],
                                    },
                                ],
                            },
                            {
                                title: "Mock 2",
                                questions: [
                                    {
                                        question: "Test question 3",
                                        answers: ["Answer 3", "Answer 4", "Answer 5"],
                                    },
                                ],
                            },
                        ],
                    },
                },
            },
        });

        const сompletedLines = wrapper.findAllComponents({ name: "CompletedLine" });
        expect(сompletedLines.length).toBe(2);
        expect(сompletedLines[0].vm.done).toBe(50);
        expect(сompletedLines[1].vm.done).toBe(0);
    });
});
