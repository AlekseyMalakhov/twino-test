import { shallowMount } from "@vue/test-utils";
import Overview from "@/views/Overview.vue";

const mockAnswers = [
    [3, 4, 3],
    [2, 2, 1],
    [2, 5, 1],
];

jest.mock("../../src/api/suitabilityTest");

describe("Overview.vue", () => {
    it("should render", () => {
        require("../../src/api/suitabilityTest").getAnswers.mockReturnValueOnce(mockAnswers);
        const wrapper = shallowMount(Overview, {
            global: {
                provide: {
                    groups: [
                        {
                            title: "Mock 1",
                            questions: [
                                {
                                    question: "Test question 1",
                                    answers: ["Answer 1", "Answer 2", "Answer 3"],
                                },
                                {
                                    question: "Test question 2",
                                    answers: ["Answer 1", "Answer 2", "Answer 3"],
                                },
                            ],
                        },
                        {
                            title: "Mock 2",
                            questions: [
                                {
                                    question: "Test question 3",
                                    answers: ["Answer 3", "Answer 4", "Answer 5"],
                                },
                            ],
                        },
                    ],
                },
            },
        });

        const GroupOverviewBlocks = wrapper.findAllComponents({ name: "GroupOverviewBlock" });
        expect(GroupOverviewBlocks.length).toBe(2);
    });
});
