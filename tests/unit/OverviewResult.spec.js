import { shallowMount } from "@vue/test-utils";
import OverviewResult from "@/components/OverviewResult.vue";

describe("OverviewResult.vue", () => {
    it("should render", () => {
        const wrapper = shallowMount(OverviewResult, {
            props: {
                question: {
                    question: "Test 1",
                    answers: ["Answer 1", "Answer 2", "Answer 3"],
                },
                answer: 1,
            },
        });

        const title = wrapper.find(".title").text();
        const answer = wrapper.find(".answer").text();

        expect(title).toBe("Test 1");
        expect(answer).toBe("Answer 2");
    });
});
