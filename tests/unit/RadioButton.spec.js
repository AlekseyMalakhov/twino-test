import { shallowMount } from "@vue/test-utils";
import RadioButton from "@/components/RadioButton.vue";

describe("RadioButton.vue", () => {
    it("should render", async () => {
        const wrapper = shallowMount(RadioButton, {
            props: {
                answer: "Test answer 1",
                index: 1,
                modelValue: 4,
            },
        });

        const radioButton = wrapper.find("input");
        await radioButton.setChecked();
        expect(radioButton.element.checked).toBeTruthy();

        const text = wrapper.find(".container").text();
        expect(text).toBe("Test answer 1");
    });
});
